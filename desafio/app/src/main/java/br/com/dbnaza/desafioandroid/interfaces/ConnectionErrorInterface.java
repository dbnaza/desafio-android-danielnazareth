package br.com.dbnaza.desafioandroid.interfaces;

public interface ConnectionErrorInterface {

    public void onRetry();

    public void onConnectionLost();
}
