package br.com.dbnaza.desafioandroid.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkBroadcast {

    private Context mContext;
    private NetworkStateReceiver mReceiver;
    private OnConnectionChange listener;

    public interface OnConnectionChange{
        void onConnectionChange(boolean isConnected);
    }

    public NetworkBroadcast() {
        mReceiver = new NetworkStateReceiver();
    }

    public void setListener(OnConnectionChange listener) {
        this.listener = listener;
    }

    public synchronized void startBroadcast(Context context) {
        mContext = context;
        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        context.registerReceiver(mReceiver, filter);
    }

    public synchronized void stopListening() {
        try {
            mContext.unregisterReceiver(mReceiver);
            mContext = null;
        }catch (Exception e){}
    }

    public class NetworkStateReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            if (intent.getExtras() != null && listener != null) {
                if (intent.getExtras().getBoolean(ConnectivityManager.EXTRA_NO_CONNECTIVITY, Boolean.FALSE)) {
                        listener.onConnectionChange(false);
                } else {
                        listener.onConnectionChange(true);
                }
            }
        }
    }
}
