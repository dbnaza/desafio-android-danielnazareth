package br.com.dbnaza.desafioandroid.app;

public class Constants {

    public static class Service {

        public static final int TIMEOUT = 18000;
        public static final int LIMIT = 30;
        public static final long SIZE_OF_CACHE = 10 * 1024 * 1024; // 10 MiB

        public static class Search {
            public static final String SEARCH_REPOSITORIES = "/search/repositories";

            public static class Queries {
                public static final String QUERY = "q";
                public static final String SORT = "sort";
                public static final String PAGE = "page";
            }

            public static class QueriesParameters {
                public static final String LANGUAGE = "language:Java";
                public static final String SORT_STAR = "stars";
            }
        }

        public static class PullRequests {
            public static final String SEARCH_PULL_REQUESTS = "repos/{" + Paths.CREATOR + "}/{" +Paths.REPOSITORY +"}/pulls";

            public static class Paths {
                public static final String CREATOR = "creator";
                public static final String REPOSITORY = "repository";
            }
        }
    }

    public class SharedPreferences {
        public static final String PATH = "path_distrito_data";
        public static final String HAS_CACHE = "has_cache";
    }
}
