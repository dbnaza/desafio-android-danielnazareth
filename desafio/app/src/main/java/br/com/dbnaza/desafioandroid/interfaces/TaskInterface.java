package br.com.dbnaza.desafioandroid.interfaces;

import okhttp3.Response;

public interface TaskInterface {

    public void doOnPre();

    public void doOnPosSuccess(retrofit2.Response<?> response);

    public void doOnPosFail();

    public void doWhenHasNoConnection();
}

