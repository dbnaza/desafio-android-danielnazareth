package br.com.dbnaza.desafioandroid.app;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.google.gson.GsonBuilder;

import java.io.File;
import java.util.concurrent.TimeUnit;

import br.com.dbnaza.desafioandroid.BuildConfig;
import br.com.dbnaza.desafioandroid.service.RestApi;
import br.com.dbnaza.desafioandroid.service.interceptors.CachingControlInterceptor;
import br.com.dbnaza.desafioandroid.utils.PreferencesManager;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static br.com.dbnaza.desafioandroid.app.Constants.Service.SIZE_OF_CACHE;

public class DesafioApp extends MultiDexApplication {

    private static RestApi service;
    private static Retrofit retrofit;
    private static DesafioApp instance;
    public static PreferencesManager prefs;

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
        initVariables(base);
    }

    public static DesafioApp getContext() {
        return instance;
    }

    public void initVariables(Context context) {
        initRetrofit(context);

        PreferencesManager.initializeInstance(context);
        prefs = PreferencesManager.getInstance();
    }

    public static  void initRetrofit(Context context){

        Cache cache = new Cache(new File(context.getCacheDir(), "http"), SIZE_OF_CACHE);

        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(Constants.Service.TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(Constants.Service.TIMEOUT, TimeUnit.SECONDS)
                .addNetworkInterceptor(new CachingControlInterceptor())
                .cache(cache)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().create()))
                .client(client)
                .build();

        service = retrofit.create(RestApi.class);
    }

    public static RestApi getService() {
        return service;
    }


    public static boolean hasCache() {
        return prefs.getBoolean(Constants.SharedPreferences.HAS_CACHE, false);
    }

    public static void setHasCache(boolean b) {
        prefs.setValue(Constants.SharedPreferences.HAS_CACHE, b);
    }
}
