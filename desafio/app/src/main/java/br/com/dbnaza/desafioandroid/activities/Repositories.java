package br.com.dbnaza.desafioandroid.activities;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import br.com.dbnaza.desafioandroid.activities.bases.ListBase;
import br.com.dbnaza.desafioandroid.adapters.RepositoriesAdapter;
import br.com.dbnaza.desafioandroid.interfaces.OnLoadMoreListener;
import br.com.dbnaza.desafioandroid.interfaces.TaskInterface;
import br.com.dbnaza.desafioandroid.objects.repositories.GitHubRepositoriesResponse;
import br.com.dbnaza.desafioandroid.objects.repositories.Repository;
import br.com.dbnaza.desafioandroid.service.Calls;
import retrofit2.Response;

public class Repositories extends ListBase implements TaskInterface, OnLoadMoreListener {

    private RepositoriesAdapter mAdapter;
    private List<Repository> mRepositories = new ArrayList<>();
    private int mPage = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getRepositories();
    }

    @Override
    public void setAdapter() {
        mAdapter = new RepositoriesAdapter(this, mRepositories, mPage, this);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onLoadMore() {
        mPage++;
        getRepositories();
    }

    @Override
    public void doOnRefresh() {
        mPage = 1;
        getRepositories();
    }

    private void getRepositories(){
        Calls.getRepositories(mPage, this);
    }

    @Override
    public void doOnPre() {
        if(isFirstPage()) {
            super.doOnPre();
        }
    }

    @Override
    public void doOnPosSuccess(Response<?> response) {
        super.doOnPosSuccess(response);

        GitHubRepositoriesResponse gitHubRepositoriesResponse = (GitHubRepositoriesResponse) response.body();

        if(isFirstPage()){
            mRepositories = gitHubRepositoriesResponse.getItems();
            if(mRepositories.size() > 0) {
                setAdapter();
                changeEmptyStatus(false);
            } else {
                changeEmptyStatus(true);
            }
        } else {
            for (Repository repository : gitHubRepositoriesResponse.getItems()) {
                mAdapter.addItem(repository);
            }

            (mAdapter).setLoaded();
            (mAdapter).setOffset(mPage);
        }

    }

    private boolean isFirstPage(){
        return mPage == 1;
    }

    @Override
    public void doOnPosFail() {
        super.doOnPosFail();
    }
}
