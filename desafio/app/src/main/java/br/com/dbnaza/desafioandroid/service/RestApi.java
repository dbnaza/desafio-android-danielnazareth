package br.com.dbnaza.desafioandroid.service;

import java.util.List;

import br.com.dbnaza.desafioandroid.objects.pullrequest.PullRequestsResponse;
import br.com.dbnaza.desafioandroid.objects.repositories.GitHubRepositoriesResponse;
import retrofit2.Call;
import retrofit2.http.GET;

import br.com.dbnaza.desafioandroid.app.Constants.Service.*;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RestApi {

    @GET(Search.SEARCH_REPOSITORIES)
    Call<GitHubRepositoriesResponse> getRepositories(@Query(Search.Queries.QUERY) String query,
                                                     @Query(Search.Queries.SORT) String sort,
                                                     @Query(Search.Queries.PAGE) int page);

    @GET(PullRequests.SEARCH_PULL_REQUESTS)
    Call<List<PullRequestsResponse>> getPullRequests(@Path(PullRequests.Paths.CREATOR) String creator,
                                                     @Path(PullRequests.Paths.REPOSITORY) String repository);
}
