package br.com.dbnaza.desafioandroid.objects.pullrequest;

import com.google.gson.annotations.SerializedName;

public class Links {
        @SerializedName("self")
        private Self self;
        @SerializedName("html")
        private Html html;
        @SerializedName("issue")
        private Issue issue;
        @SerializedName("comments")
        private Comments comments;
        @SerializedName("review_comments")
        private ReviewComments reviewComments;
        @SerializedName("review_comment")
        private ReviewComment reviewComment;
        @SerializedName("commits")
        private Commits commits;
        @SerializedName("statuses")
        private Statuses statuses;

        public Self getSelf() {
            return self;
        }

        public void setSelf(Self self) {
            this.self = self;
        }

        public Html getHtml() {
            return html;
        }

        public void setHtml(Html html) {
            this.html = html;
        }

        public Issue getIssue() {
            return issue;
        }

        public void setIssue(Issue issue) {
            this.issue = issue;
        }

        public Comments getComments() {
            return comments;
        }

        public void setComments(Comments comments) {
            this.comments = comments;
        }

        public ReviewComments getReviewComments() {
            return reviewComments;
        }

        public void setReviewComments(ReviewComments reviewComments) {
            this.reviewComments = reviewComments;
        }

        public ReviewComment getReviewComment() {
            return reviewComment;
        }

        public void setReviewComment(ReviewComment reviewComment) {
            this.reviewComment = reviewComment;
        }

        public Commits getCommits() {
            return commits;
        }

        public void setCommits(Commits commits) {
            this.commits = commits;
        }

        public Statuses getStatuses() {
            return statuses;
        }

        public void setStatuses(Statuses statuses) {
            this.statuses = statuses;
        }

        public static class Self {
            @SerializedName("href")
            private String href;

            public String getHref() {
                return href;
            }

            public void setHref(String href) {
                this.href = href;
            }
        }

        public static class Html {
            @SerializedName("href")
            private String href;

            public String getHref() {
                return href;
            }

            public void setHref(String href) {
                this.href = href;
            }
        }

        public static class Issue {
            @SerializedName("href")
            private String href;

            public String getHref() {
                return href;
            }

            public void setHref(String href) {
                this.href = href;
            }
        }

        public static class Comments {
            @SerializedName("href")
            private String href;

            public String getHref() {
                return href;
            }

            public void setHref(String href) {
                this.href = href;
            }
        }

        public static class ReviewComments {
            @SerializedName("href")
            private String href;

            public String getHref() {
                return href;
            }

            public void setHref(String href) {
                this.href = href;
            }
        }

        public static class ReviewComment {
            @SerializedName("href")
            private String href;

            public String getHref() {
                return href;
            }

            public void setHref(String href) {
                this.href = href;
            }
        }

        public static class Commits {
            @SerializedName("href")
            private String href;

            public String getHref() {
                return href;
            }

            public void setHref(String href) {
                this.href = href;
            }
        }

        public static class Statuses {
            @SerializedName("href")
            private String href;

            public String getHref() {
                return href;
            }

            public void setHref(String href) {
                this.href = href;
            }
        }
    }
