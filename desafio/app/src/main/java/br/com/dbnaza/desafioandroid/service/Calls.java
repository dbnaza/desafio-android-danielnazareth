package br.com.dbnaza.desafioandroid.service;

import java.util.List;

import br.com.dbnaza.desafioandroid.app.DesafioApp;
import br.com.dbnaza.desafioandroid.interfaces.TaskInterface;
import br.com.dbnaza.desafioandroid.objects.pullrequest.PullRequestsResponse;
import br.com.dbnaza.desafioandroid.objects.repositories.GitHubRepositoriesResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import br.com.dbnaza.desafioandroid.app.Constants.Service.Search.*;

public class Calls {

    public static void getRepositories(final int page, final TaskInterface listener){
        listener.doOnPre();

        Call<GitHubRepositoriesResponse> call = DesafioApp.getService().getRepositories(QueriesParameters.LANGUAGE,
                QueriesParameters.SORT_STAR, page);

        call.enqueue(new Callback<GitHubRepositoriesResponse>() {
            @Override
            public void onResponse(Call<GitHubRepositoriesResponse> call, Response<GitHubRepositoriesResponse> response) {
                if (response.isSuccessful()) {
                    DesafioApp.setHasCache(true);
                    listener.doOnPosSuccess(response);
                } else {
                    listener.doOnPosFail();
                }
            }

            @Override
            public void onFailure(Call<GitHubRepositoriesResponse> call, Throwable t) {
                DesafioApp.setHasCache(false);
                listener.doWhenHasNoConnection();
            }
        });
    }

    public static void getPullRequests(final String creator, String repo, final TaskInterface listener){
        listener.doOnPre();

        Call<List<PullRequestsResponse>> call = DesafioApp.getService().getPullRequests(creator, repo);

        call.enqueue(new Callback<List<PullRequestsResponse>>() {
            @Override
            public void onResponse(Call<List<PullRequestsResponse>> call, Response<List<PullRequestsResponse>> response) {
                if (response.isSuccessful()) {
                    DesafioApp.setHasCache(true);
                    listener.doOnPosSuccess(response);
                } else {
                    listener.doOnPosFail();
                }
            }

            @Override
            public void onFailure(Call<List<PullRequestsResponse>> call, Throwable t) {
                DesafioApp.setHasCache(false);
                listener.doWhenHasNoConnection();
            }
        });
    }
}
