package br.com.dbnaza.desafioandroid.activities.bases;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import br.com.dbnaza.desafioandroid.R;
import br.com.dbnaza.desafioandroid.app.DesafioApp;
import br.com.dbnaza.desafioandroid.interfaces.ConnectionErrorInterface;
import butterknife.Bind;
import butterknife.ButterKnife;

public abstract class ListBase extends BaseActivity implements ConnectionErrorInterface{

    @Bind(R.id.my_recycler_view)
    protected RecyclerView mRecyclerView;
    @Bind(R.id.swipe)
    protected SwipeRefreshLayout mSwipe;
    @Bind(R.id.txt_empty)
    protected TextView mTxtEmpty;
    @Bind(R.id.empty_view)
    protected LinearLayout mEmptyView;
    @Bind(R.id.connection_failed_view)
    protected LinearLayout mConnectionFailedView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repositories);
        ButterKnife.bind(this);

        setParentConnectionChangedListener(this);
        setLayoutManager();
        setSwipeRefreshEnabledConfig();
    }

    private void setLayoutManager(){
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void setViewsEnabled(boolean enabled) {
        changeSwipeVisibility(enabled);
    }

    public void setSwipeRefreshEnabledConfig(){
        mSwipe.setColorSchemeResources(R.color.colorPrimary);
        mSwipe.setEnabled(true);
        mSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                doOnRefresh();
            }
        });
    }

    public void changeSwipeVisibility(final boolean enabled){
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                mSwipe.setRefreshing(!enabled);
            }
        });
    }

    @Override
    public void onRetry() {
        if(mRecyclerView.getAdapter() == null) {
            doOnRefresh();
        }

        changeConnectionStatus(true);
    }

    @Override
    public void onConnectionLost() {
        if(!DesafioApp.hasCache()) {
            changeConnectionStatus(false);
        }
    }

    private void changeConnectionStatus(boolean hasConnection){
        mRecyclerView.setVisibility(hasConnection ? View.VISIBLE : View.GONE);
        mConnectionFailedView.setVisibility(hasConnection ? View.GONE : View.VISIBLE);
    }

    protected void changeEmptyStatus(boolean isEmpty){
        mEmptyView.setVisibility(isEmpty ? View.VISIBLE : View.GONE);
        mRecyclerView.setVisibility(isEmpty ? View.GONE : View.VISIBLE);
    }

    public abstract void setAdapter();

    public abstract void doOnRefresh();
}
