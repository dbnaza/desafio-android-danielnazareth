package br.com.dbnaza.desafioandroid.utils;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import br.com.dbnaza.desafioandroid.R;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class ImageUtils {

    public static void loadImage(Context context, String url, ImageView imgUser){
        Glide.with(context)
                .load(url)
                .bitmapTransform(new CropCircleTransformation(context))
                .placeholder(R.drawable.ic_avatar)
                .into(imgUser);
    }

}
