package br.com.dbnaza.desafioandroid.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;

import br.com.dbnaza.desafioandroid.R;
import br.com.dbnaza.desafioandroid.activities.PullRequests;
import br.com.dbnaza.desafioandroid.adapters.holders.ItemViewHolder;
import br.com.dbnaza.desafioandroid.adapters.holders.ProgressHolder;
import br.com.dbnaza.desafioandroid.app.Constants;
import br.com.dbnaza.desafioandroid.interfaces.OnLoadMoreListener;
import br.com.dbnaza.desafioandroid.objects.repositories.Repository;
import br.com.dbnaza.desafioandroid.utils.ImageUtils;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class RepositoriesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private LayoutInflater mInflater;
    private Context mContext;
    private List<Repository> mItems;

    private static final int TYPE_ITEM = 1;
    private final int TYPE_LOADING = 0;

    private int mOffset = 1;
    private boolean mLoading;

    private OnLoadMoreListener mOnLoadMoreListener;

    //Constructor
    public RepositoriesAdapter(Context context, List<Repository> items, int offset, OnLoadMoreListener onLoadMoreListener) {
        mItems = items;
        mOnLoadMoreListener = onLoadMoreListener;
        mContext = context;
        mOffset = offset;
    }

    //Views Methods
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {

        if (mInflater == null) {
            mInflater = LayoutInflater.from(parent.getContext());
        }

        if (viewType == TYPE_ITEM) {
            View v = mInflater.inflate(R.layout.item_repository, parent, false);
            return new ItemViewHolder(v);
        } else {
            View v = mInflater.inflate(R.layout.loading, parent, false);
            return new ProgressHolder(v);
        }

    }

    private void loadImage(ImageView imgUser, String url) {
        ImageUtils.loadImage(mContext, url, imgUser);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof ItemViewHolder) {

            ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

            final Repository item = mItems.get(position);

            itemViewHolder.txtRepositoryName.setText(item.getName());
            itemViewHolder.txtRepositoryDesc.setText(item.getDescription());
            itemViewHolder.txtRepositoryForks.setText(String.valueOf(item.getForks()));
            itemViewHolder.txtRepositoryStars.setText(String.valueOf(item.getStargazersCount()));

            loadImage(itemViewHolder.imgUser, item.getOwner().getAvatarUrl());
            itemViewHolder.txtUsername.setText(item.getOwner().getLogin());

            itemViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   startDetails(item);
                }
            });
        }

        if (!mLoading && position + 1 == mOffset * Constants.Service.LIMIT) {
            mOnLoadMoreListener.onLoadMore();
            mLoading = true;
        }
    }

    private void startDetails(Repository repository) {
        PullRequests.start(mContext, repository.getOwner().getLogin(), repository.getName());
    }

    public void setLoaded() {
        mLoading = false;
    }

    public void setOffset(int mOffset) {
        this.mOffset = mOffset;
    }

    @Override
    public int getItemViewType(int position) {

        return mLoading & position >= mItems.size()
                ? TYPE_LOADING
                : TYPE_ITEM;
    }


    @Override
    public int getItemCount() {
        return mItems.size() + (mLoading ? 1 : 0);
    }

    public void addItem(Repository repository){
        mItems.add(repository);
        notifyItemInserted(getItemCount());
    }
}