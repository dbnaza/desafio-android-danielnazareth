package br.com.dbnaza.desafioandroid.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.util.List;

import br.com.dbnaza.desafioandroid.R;
import br.com.dbnaza.desafioandroid.adapters.holders.ItemViewHolder;
import br.com.dbnaza.desafioandroid.adapters.holders.ProgressHolder;
import br.com.dbnaza.desafioandroid.app.Constants;
import br.com.dbnaza.desafioandroid.interfaces.OnLoadMoreListener;
import br.com.dbnaza.desafioandroid.objects.pullrequest.PullRequestsResponse;
import br.com.dbnaza.desafioandroid.utils.ImageUtils;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class PullRequestsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private LayoutInflater mInflater;
    private Context mContext;
    private List<PullRequestsResponse> mItems;

    private static final int TYPE_ITEM = 1;
    private final int TYPE_LOADING = 0;

    private int mOffset = 1;
    private boolean mLoading;

    private OnLoadMoreListener mOnLoadMoreListener;

    //Constructor
    public PullRequestsAdapter(Context context, List<PullRequestsResponse> items, int offset, OnLoadMoreListener onLoadMoreListener) {
        mItems = items;
        mOnLoadMoreListener = onLoadMoreListener;
        mContext = context;
        mOffset = offset;
        mLoading = false;
    }

    //Views Methods
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {

        if (mInflater == null) {
            mInflater = LayoutInflater.from(parent.getContext());
        }

        if (viewType == TYPE_ITEM) {
            View v = mInflater.inflate(R.layout.item_pull_requests, parent, false);
            return new ItemViewHolder(v);
        } else {
            View v = mInflater.inflate(R.layout.loading, parent, false);
            return new ProgressHolder(v);
        }

    }

    private void loadImage(ImageView imgUser, String url) {
        ImageUtils.loadImage(mContext, url, imgUser);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof ItemViewHolder) {
            ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

            final PullRequestsResponse item = mItems.get(position);
            DateTime createdDateTime = new DateTime(item.getCreatedAt());

            itemViewHolder.txtRepositoryName.setText(item.getTitle());
            itemViewHolder.txtRepositoryDesc.setText(item.getBody());
            itemViewHolder.txtDate.setText(DateTimeFormat.forPattern("HH:mm dd/MM/yyyy")
                                                        .print(createdDateTime));
            itemViewHolder.linNumericInfo.setVisibility(View.GONE);

            loadImage(itemViewHolder.imgUser, item.getUser().getAvatarUrl());
            itemViewHolder.txtUsername.setText(item.getUser().getLogin());

            itemViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startDetails(item.getHtmlUrl());
                }
            });
        }

        if (!mLoading && position + 1 == mOffset * Constants.Service.LIMIT) {
            mOnLoadMoreListener.onLoadMore();
            mLoading = true;
        }
    }

    private void startDetails(String url) {
        if(!TextUtils.isEmpty(url)){
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            mContext.startActivity(browserIntent);
        }
    }

    public void setLoaded() {
        mLoading = false;
    }

    public void setOffset(int mOffset) {
        this.mOffset = mOffset;
    }

    @Override
    public int getItemViewType(int position) {

        return mLoading & position >= mItems.size()
                ? TYPE_LOADING
                : TYPE_ITEM;
    }


    @Override
    public int getItemCount() {
        return mItems.size() + (mLoading ? 1 : 0);
    }

    public void addItem(PullRequestsResponse pullRequestsResponse){
        mItems.add(pullRequestsResponse);
        notifyItemInserted(getItemCount());
    }
}