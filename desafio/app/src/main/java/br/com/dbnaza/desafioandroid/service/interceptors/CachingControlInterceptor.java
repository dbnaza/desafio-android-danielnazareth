package br.com.dbnaza.desafioandroid.service.interceptors;

import java.io.IOException;

import br.com.dbnaza.desafioandroid.app.DesafioApp;
import br.com.dbnaza.desafioandroid.utils.NetworkUtils;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class CachingControlInterceptor implements Interceptor {
    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request request = chain.request();

        if (request.method().equals("GET")) {
            if (NetworkUtils.isOnline(DesafioApp.getContext())) {
                request = request.newBuilder()
                        .header("Cache-Control", "only-if-cached")
                        .build();
            } else {
                request = request.newBuilder()
                        .header("Cache-Control", "public, max-stale=2419200")
                        .build();
            }
        }

        Response originalResponse = chain.proceed(request);
        return originalResponse.newBuilder()
                .header("Cache-Control", "max-age=600")
                .build();
    }
}