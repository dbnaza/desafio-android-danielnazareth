package br.com.dbnaza.desafioandroid.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import br.com.dbnaza.desafioandroid.activities.bases.ListBase;
import br.com.dbnaza.desafioandroid.adapters.PullRequestsAdapter;
import br.com.dbnaza.desafioandroid.interfaces.OnLoadMoreListener;
import br.com.dbnaza.desafioandroid.interfaces.TaskInterface;
import br.com.dbnaza.desafioandroid.objects.pullrequest.PullRequestsResponse;
import br.com.dbnaza.desafioandroid.service.Calls;
import retrofit2.Response;

public class PullRequests extends ListBase implements TaskInterface, OnLoadMoreListener {

    private static final String PULL_REQUEST_CREATOR = "CREATOR";
    private static final String PULL_REQUEST_REPO = "REPO";

    private PullRequestsAdapter mAdapter;
    private List<PullRequestsResponse> mPullRequests = new ArrayList<>();
    private int mPage = 1;
    private String repo;
    private String creator;

    public static void start(Context context, String creator, String repo) {
        Intent intent = new Intent(context, PullRequests.class);
        intent.putExtra(PULL_REQUEST_CREATOR, creator);
        intent.putExtra(PULL_REQUEST_REPO, repo);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initVariables();
        configureActionBar();
        getPullRequests();
    }

    private void configureActionBar() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            setTitle(repo);
        }
    }

    private void initVariables(){
        if(getIntent() != null && getIntent().getExtras() != null){
            creator = getIntent().getStringExtra(PULL_REQUEST_CREATOR);
            repo = getIntent().getStringExtra(PULL_REQUEST_REPO);
        }
    }

    @Override
    public void setAdapter() {
        mAdapter = new PullRequestsAdapter(this, mPullRequests, mPage, this);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onLoadMore() {
        mPage++;
        getPullRequests();
    }

    @Override
    public void doOnRefresh() {
        mPage = 1;
        getPullRequests();
    }

    private void getPullRequests(){
        Calls.getPullRequests(creator, repo, this);
    }

    @Override
    public void doOnPre() {
        if(isFirstPage()) {
            super.doOnPre();
        }
    }

    @Override
    public void doOnPosSuccess(Response<?> response) {
        super.doOnPosSuccess(response);

        List<PullRequestsResponse> pullRequestsResponses = (List<PullRequestsResponse>) response.body();

        if(isFirstPage()){
            mPullRequests = pullRequestsResponses;
            if(mPullRequests.size() > 0) {
                setAdapter();
                changeEmptyStatus(false);
            } else {
                changeEmptyStatus(true);
            }
        } else {
            for (PullRequestsResponse pullRequest : pullRequestsResponses) {
                mAdapter.addItem(pullRequest);
            }

            (mAdapter).setLoaded();
            (mAdapter).setOffset(mPage);
        }

    }

    private boolean isFirstPage(){
        return mPage == 1;
    }

    @Override
    public void doOnPosFail() {
        super.doOnPosFail();
    }
}
