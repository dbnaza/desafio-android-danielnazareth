package br.com.dbnaza.desafioandroid.activities.bases;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.ViewGroup;

import br.com.dbnaza.desafioandroid.R;
import br.com.dbnaza.desafioandroid.interfaces.ConnectionErrorInterface;
import br.com.dbnaza.desafioandroid.interfaces.TaskInterface;
import br.com.dbnaza.desafioandroid.service.NetworkBroadcast;
import butterknife.Bind;
import retrofit2.Response;

public abstract class BaseActivity extends AppCompatActivity implements TaskInterface, NetworkBroadcast.OnConnectionChange{

    public @Bind(android.R.id.content)
    ViewGroup rootView;

    private static NetworkBroadcast mNetworkBroadcast;
    protected ConnectionErrorInterface mConnectionErrorInterface;

    protected void setNetworkListener(){
        mNetworkBroadcast = new NetworkBroadcast();
        mNetworkBroadcast.setListener(this);
    }

    protected void setParentConnectionChangedListener(ConnectionErrorInterface connectionErrorInterface){
        setNetworkListener();
        mConnectionErrorInterface = connectionErrorInterface;

        if(mConnectionErrorInterface != null){
            mNetworkBroadcast.startBroadcast(this);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if(mConnectionErrorInterface != null && mNetworkBroadcast != null){
                mNetworkBroadcast.stopListening();
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionChange(boolean isConnected) {
        if(isConnected){
            mConnectionErrorInterface.onRetry();
        } else {
            mConnectionErrorInterface.onConnectionLost();
        }
    }

    @Override
    public void doOnPre() {
        setViewsEnabled(false);
    }

    @Override
    public void doOnPosSuccess(Response<?> response) {
        setViewsEnabled(true);
    }

    @Override
    public void doOnPosFail() {
        setViewsEnabled(true);
    }

    @Override
    public void doWhenHasNoConnection() {
        setViewsEnabled(true);

        Snackbar.make(findViewById(android.R.id.content),
                R.string.connection_failed_message,
                Snackbar.LENGTH_LONG)
                .show();
    }

    protected abstract void setViewsEnabled(final boolean enabled);
}
